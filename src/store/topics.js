// Utilities
import { defineStore } from 'pinia'

export const useTopicStore = defineStore('topic', {
  state: () => ({
    selected: {}
  }),
  actions: {
    setSelectedTopic(topic) {
      this.selected = topic
    }
  }
})
